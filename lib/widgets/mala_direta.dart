import 'package:flutter/material.dart';
import 'package:krakenapp/pages/content_screen.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:krakenapp/stores/mala_direta_store.dart';



class MalaDireta extends StatefulWidget {

  final String title;
  const MalaDireta ({Key key, this.title}) : super (key: key);



  @override
  State<StatefulWidget> createState() => _MalaDiretaState();
}

class _MalaDiretaState extends State<MalaDireta> {

  MalaDiretaStore malaDiretaStore = MalaDiretaStore();




  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 16,
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Título"
                              ),
                            ),
                            SizedBox(height: 34,),
                            TextFormField(
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                hintText: "Mensagem",
                                labelText: "Mensagem",
                              ),
                              maxLines: 3,
                            ),
                            SizedBox(height: 20,),
                            DropdownButtonFormField(
                              decoration: InputDecoration(
                                  labelText: "Destinatário"
                              ),
                                ),
                              SizedBox(
                                height: 34,
                              ),
                            RaisedButton(
                              color: Colors.deepPurple,
                              textColor: Colors.white,
                              child: Text('Enviar'),
                              onPressed: () {},
                            ),

                          ],
                        ),
                      ),
                    ),

                ],
              ),
            ),
        ),
        ),
      ),
    );
  }
}

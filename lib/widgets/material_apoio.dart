

import 'package:flutter/material.dart';
import 'package:krakenapp/pages/content_screen.dart';



class MatApoio extends StatefulWidget {

  final String title;
  const MatApoio ({Key key, this.title}) : super (key: key);

  @override
  State<StatefulWidget> createState() => _MatApoioState();
}

class _MatApoioState extends State<MatApoio> {

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          child: Column(
            children: <Widget>[

              Expanded(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0),
                  ),
                  elevation: 16,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView.separated(
                            itemCount: 12,
                            itemBuilder: (_, index){
                              return ListTile(
                                trailing: Icon(Icons.keyboard_arrow_right),
                                title: Text('Aula $index' , textAlign: TextAlign.left,),
                                onTap: (){
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context)=>ContentScreen())
                                  );
                                },
                              );
                            },
                            separatorBuilder: (_, __){
                              return Divider();
                            },

                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

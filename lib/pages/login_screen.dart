import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:krakenapp/stores/login_store.dart';
import 'package:krakenapp/widgets/custom_text_field.dart';
import 'package:krakenapp/widgets/custom_icon_button.dart';
import 'package:krakenapp/pages/list_screen.dart';
import 'package:mobx/mobx.dart';


class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  LoginStore loginStore = LoginStore();

  ReactionDisposer disposer;


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    disposer = reaction(
       (_) => loginStore.loggedIn,
       (loggedIn){
          if(loggedIn)
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (_)=>ListScreen())
            );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(left: 32, right: 32),
          child: SingleChildScrollView(
            child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                elevation: 16,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        width: 108,
                        height: 108,
                        child: Image.asset("assets/images/logo-kraken.png"),
                      ),
                      Observer(
                        builder: (_){
                          return CustomTextField(
                            hint: 'E-mail',
                            prefix: Icon(Icons.account_circle),
                            textInputType: TextInputType.emailAddress,
                            onChanged: loginStore.setEmail,
                            enabled: !loginStore.loading,
                          );
                        },
                      ),
                      const SizedBox(height: 16,),
                      Observer(
                        builder: (_){
                          return CustomTextField(
                            hint: 'Senha',
                            prefix: Icon(Icons.lock),
                            obscure: !loginStore.passwordVisible,
                            onChanged: loginStore.setPassword,
                            enabled: !loginStore.loading,
                            suffix: CustomIconButton(
                              radius: 32,
                              iconData: loginStore.passwordVisible ? Icons.visibility_off : Icons.visibility,
                              onTap: loginStore.togglePasswordVisibility,
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 3, bottom: 4, top: 6),
                        child: GestureDetector(
                          child: Text(
                            'Esqueceu sua senha?',
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          onTap: () {
                          },
                        ),
                      ),
                      const SizedBox(height: 8,),
                      Observer(
                        builder: (_){
                          return Container(
                            height: 40,
                            child: RaisedButton(
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: loginStore.loading ?
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(Colors.white),
                              ) : Text('Login'),
                              disabledColor: Theme.of(context).primaryColor.withAlpha(100),
                              textColor: Colors.white,
                              onPressed:  loginStore.loginPressed,
                              elevation: 0,
                            ),
                          );
                        },
                      ),
                      const SizedBox(height: 8,),
                      Divider(color: Colors.grey[900]),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Wrap(
                          alignment: WrapAlignment.center,
                          children: <Widget>[
                            const Text(
                              'Não possui uma conta? ',
                              style:  TextStyle(fontSize: 14),
                            ),
                            GestureDetector(
                              onTap: (){
                              },
                              child: Text(
                                'Acesse aqui',
                                style: TextStyle(
                                  color: Colors.grey[900],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    disposer();
    super.dispose();
  }


}

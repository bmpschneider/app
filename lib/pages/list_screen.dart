import 'package:flutter/material.dart';
import 'package:krakenapp/widgets/custom_icon_button.dart';
import 'package:krakenapp/widgets/custom_text_field.dart';

import 'login_screen.dart';
import 'content_screen.dart';

class ListScreen extends StatefulWidget {

  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          margin: const EdgeInsets.fromLTRB(32, 0, 32, 32),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      ' Kraken - Udesc',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 32
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.exit_to_app),
                      color: Colors.white,
                      onPressed: (){
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context)=>LoginScreen())
                        );
                      },
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  elevation: 16,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Selecione a matéria',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                          ),
                        ),
                        const SizedBox(height: 8,),
                        Expanded(
                          child: ListView.separated(
                            itemCount: 12,
                            itemBuilder: (_, index){
                              return ListTile(
                                trailing: Icon(Icons.keyboard_arrow_right),
                                title: Text('Matéria $index' , textAlign: TextAlign.left,),
                                onTap: (){
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context)=>ContentScreen())
                                  );
                                },
                              );
                            },
                            separatorBuilder: (_, __){
                              return Divider();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krakenapp/widgets/custom_icon_button.dart';
import 'package:krakenapp/widgets/custom_text_field.dart';
import 'package:krakenapp/widgets/mala_direta.dart';
import 'package:krakenapp/widgets/material_apoio.dart';
import 'package:krakenapp/widgets/mala_direta.dart';
import 'package:krakenapp/widgets/trabalho_colaborativo.dart';

import 'list_screen.dart';
import 'login_screen.dart';



class ContentScreen extends StatefulWidget {

  @override
  _ContentScreenState createState() => _ContentScreenState();
}

class _ContentScreenState extends State<ContentScreen> {

    @override
    Widget build(BuildContext context) {

      return DefaultTabController(
                 child: Scaffold(
                    appBar: AppBar(
                          actions: <Widget>[
                            IconButton(
                              icon: Icon(Icons.arrow_back_ios),
                              color: Colors.white,
                              onPressed: (){
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(builder: (context)=>ListScreen())
                              );
                            },
                          ),
                        ],
                        bottom: TabBar(
                          tabs: <Widget>[
                            Tab(text: "Mala Direta", icon: Icon(Icons.business_center),),
                            Tab(text: "Mat. de Apoio", icon: Icon(Icons.thumb_up),),
                            Tab(text: "Trab. Colaborat.", icon: Icon(Icons.people),),
                          ],
                        ),
                      ),

                    body: TabBarView(
                      children: <Widget>[
                        Center(child: MalaDireta()),
                        Center(child: MatApoio()),
                        Center(child: TrabalhoColaborativo()),
                      ],
                    )),
              length: 3,
      );
    }
}

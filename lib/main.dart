import 'package:flutter/material.dart';
import 'package:krakenapp/pages/login.page.dart';
import 'package:krakenapp/pages/login_screen.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kraken App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        scaffoldBackgroundColor: Colors.deepPurple
      ),
      home: LoginScreen(),
    );
  }
}

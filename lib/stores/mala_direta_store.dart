import 'package:mobx/mobx.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
part 'mala_direta_store.g.dart';

class MalaDiretaStore = _MalaDiretaStore with _$MalaDiretaStore;

  abstract class _MalaDiretaStore with Store {



    @observable
    String titulo;

    @action
    changeTitle(String newTitle) => titulo = newTitle;

    @observable
    String mensagem;

    @action
    changeMsg(String newMsg) => mensagem = newMsg;


  }